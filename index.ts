// Actual message of being extracted from raw events
type Message = {
	 sender: string,
	 receiver: Array<string> | null,
	 content: string
}

// SlackEvent
type SlackEvent = {
	 type: string
};


// slack message changed
// https://api.slack.com/events/message/message_changed
//
// slack message replied
// https://api.slack.com/events/message/message_replied
//
// slack channel join
// https://api.slack.com/events/message/channel_join
//
// slack channel leave
// https://api.slack.com/events/message/channel_leave
//
// slack thread broadcast
// https://api.slack.com/events/message/thread_broadcast
//
// slack group join
// https://api.slack.com/events/message/group_join
//
// slack group leave
// https://api.slack.com/events/message/group_leave
//
// slack message channel
// https://api.slack.com/events/message.channels
//

type SlackChannelMessage = SlackEvent & {
	 channel: string,
	 user: string,
	 text: string,
	 ts: string,
	 event_ts: string,
	 channel_type: string,
};

type SlackEventType = SlackChannelMessage;

type SlackTransform = (event: SlackEventType) => Message;

// This function extract message where receiver might not exists
// to message that has receiver, we could infer multiple receivers
// by extracting from its content
type ExtractMessage = (message: Message) => Message;

// Query for historically whether given range of dates (time -> time)
// what is the number
//
type HistoryQuery = (range: [Date, Date]) => AsyncIterableIterator<[Date, Number]>;

// Energy interpolation function for given time with
// given historical query
type EnergyInterp = (time: Date, query: HistoryQuery) => AsyncIterableIterator<Number>;

// This resolve for given group to array of users
type GroupResolver = (group: string) => AsyncIterableIterator<string>;

// This resolve for given user to alias of user
type AliasResolver = (user: string) => AsyncIterableIterator<string>;

// This resolve given event hook (task)
// How much churn of number it will impact to given user
// implied from message
type CalculateFn =(
	 time: Date,
	 room: string,
	 message: Message,
	 energy: Number,
	 query: HistoryQuery,
) => AsyncIterableIterator<Number>;



(async () => {


})()
